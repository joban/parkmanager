import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-edit-floor',
  templateUrl: './edit-floor.component.html',
  styleUrls: ['./edit-floor.component.css']
})
export class EditFloorComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: EditFloorDataSource;
  //p:number=1;
  //listeEditFloor:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<EditFloorComponent>,
    private httpSv:HttpService,
  @Inject(MAT_DIALOG_DATA) public dataToUpdate) { 
    this.data=Object.assign({},dataToUpdate)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  editFloor(){
    if(this.data.name){
        this.httpSv.update('floor',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Floor has been successfully edited')
            this.dialogRef.close({edited:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.name) this.snackBarService.openSnackBar('Enter floor name please');
    }
  }
  ngOnInit() {
    //this.dataSource = new EditFloorDataSource(this.paginator, this.sort);
    //this.getEditFloor()
  }
}
