import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-create-floor',
  templateUrl: './create-floor.component.html',
  styleUrls: ['./create-floor.component.css']
})
export class CreateFloorComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: CreateFloorDataSource;
  //p:number=1;
  //listeCreateFloor:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<CreateFloorComponent>,
    private httpSv:HttpService,
  /*@Inject(MAT_DIALOG_DATA) public data*/) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createFloor(){
    if(this.data.name){
        this.httpSv.create('floor',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Floor has been successfully created')
            this.dialogRef.close({created:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.name) this.snackBarService.openSnackBar('Enter floor name please');
    }
  }
  ngOnInit() {
    //this.dataSource = new CreateFloorDataSource(this.paginator, this.sort);
    //this.getCreateFloor()
  }
}
