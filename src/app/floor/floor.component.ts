import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { HttpService } from '../api/http.service';
import { CreateFloorComponent } from './create-floor/create-floor.component';
import { EditFloorComponent } from './edit-floor/edit-floor.component';
import { ConfirmationDialogComponent } from '../confimation-dialog/confirmation-dialog.component';
import { SnackbarService } from '../service/snackbar/snackbar.service';

@Component({
  selector: 'app-floor',
  templateUrl: './floor.component.html',
  styleUrls: ['./floor.component.css']
})
export class FloorComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: FloorDataSource;
    listeFloor:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','action'];
  constructor(private httpSv:HttpService,public dialog: MatDialog,private snackBarService:SnackbarService){
  }
  getFloor(){
    this.httpSv.geAll('floor').subscribe(res=>{
      this.listeFloor=res.items ? res.items : []
    },
  error=>{
    this.listeFloor=[]
  })
  }
  deleteFloor(data){
    this.httpSv.delete('floor',data).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("Floor has been successfully deleted")
        this.getFloor()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  openCreateFloorModal(): void {
    const dialogRef = this.dialog.open(CreateFloorComponent, {
      width: '500px'//,
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      //this.animal = result;
      if(result.created) this.getFloor()
    });
  }
  openEditFloorModal(data): void {
    const dialogRef = this.dialog.open(EditFloorComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {

      if(result.edited) this.getFloor()
    });
  }
  confirmDeleteFloor(row): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: 'Are you sure to delete this floor ?'
    });

    dialogRef.beforeClosed().subscribe(result => {

      if(result) this.deleteFloor(row)
    });
  }
  ngOnInit() {
    //this.dataSource = new FloorDataSource(this.paginator, this.sort);
    this.getFloor()
  }
}
