import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../api/http.service';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: ConfirmationDialogDataSource;
  //p:number=1;
  //listeConfirmationDialog:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  constructor(
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    private httpSv:HttpService,
  @Inject(MAT_DIALOG_DATA) public message) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    //this.dataSource = new ConfirmationDialogDataSource(this.paginator, this.sort);
    //this.getConfirmationDialog()
  }
}
