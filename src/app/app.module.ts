import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatSnackBarModule, MAT_DIALOG_DEFAULT_OPTIONS, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FloorComponent } from './floor/floor.component';
import { SpotComponent } from './spot/spot.component';
import { UserComponent } from './user/user.component';
import { RoleComponent } from './role/role.component';
import { ClientComponent } from './client/client.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './api/http.service';
import { CreateRoleComponent } from './role/create-role/create-role.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditRoleComponent } from './role/edit-role/edit-role.component';
import { SnackbarService } from './service/snackbar/snackbar.service';
import { ConfirmationDialogComponent } from './confimation-dialog/confirmation-dialog.component';
import { CreateFloorComponent } from './floor/create-floor/create-floor.component';
import { EditFloorComponent } from './floor/edit-floor/edit-floor.component';
import { EditSpotComponent } from './spot/edit-spot/edit-spot.component';
import { CreateSpotComponent } from './spot/create-spot/create-spot.component';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoginComponent } from './login/login.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { CreateUserComponent } from './user/create-user/create-user.component';
@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    FloorComponent,
    SpotComponent,
    UserComponent,
    RoleComponent,
    ClientComponent,
    CreateRoleComponent,
    EditRoleComponent,
    CreateFloorComponent,
    EditFloorComponent,
    CreateSpotComponent,
    EditSpotComponent,

    CreateUserComponent,
    EditUserComponent,
    ConfirmationDialogComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    LoadingBarHttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  entryComponents:[
    CreateRoleComponent,
    EditRoleComponent,
    CreateFloorComponent,
    EditFloorComponent,
    CreateSpotComponent,
    EditSpotComponent,
    CreateUserComponent,
    EditUserComponent,
    ConfirmationDialogComponent
  ],
  providers: [
    SnackbarService,
    HttpService//,
  //{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
