import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SnackbarService {

  constructor(private _snackBar:MatSnackBar) { }
  openSnackBar(message:string){
    this._snackBar.open(message, 'close', {
      duration: 2000,
    });
  }
}
