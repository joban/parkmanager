import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FloorComponent } from './floor/floor.component';
import { SpotComponent } from './spot/spot.component';
import { UserComponent } from './user/user.component';
import { RoleComponent } from './role/role.component';
import { ClientComponent } from './client/client.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:'',
    pathMatch:'full',
    redirectTo:"auth"
  },
  {
    'path':'menu',
    component:NavigationComponent,
    children:[
      {
        'path':'dashboard',
        component:DashboardComponent
      },
      {
        'path':'floor',
        component:FloorComponent
      },
      {
        'path':'spot',
        component:SpotComponent
      },
      {
        'path':'user',
        component:UserComponent
      },
      {
        'path':'role',
        component:RoleComponent
      },
      {
        'path':'client',
        component:ClientComponent
      }

    ]
  },
  {
    path:'auth',
    children:[
      {
        path:'',
        pathMatch:'full',
        redirectTo:'login'
      },
      {
        path:"login",
        component:LoginComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
