import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../api/http.service';
import { SnackbarService } from '../service/snackbar/snackbar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  form: FormGroup = new FormGroup({
    username: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
  });
  constructor(private httpSv:HttpService,private snackBarService:SnackbarService,private router:Router){

  }

  login() {
    if (this.form.valid) {
      //this.submitEM.emit(this.form.value);
      this.httpSv.login(this.form.value).subscribe(res=>{
          if(!res.error){
            sessionStorage.setItem('loggedUser',JSON.stringify(res.items[0]))
            this.router.navigate(['/menu/spot'])
          }
          else this.snackBarService.openSnackBar(res.message)
      },error=>{
          this.snackBarService.openSnackBar(error)
      })
      
    }
  }
  // @Input() error: string | null;

  // @Output() submitEM = new EventEmitter();
}
