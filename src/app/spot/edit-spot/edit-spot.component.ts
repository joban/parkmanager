import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-edit-spot',
  templateUrl: './edit-spot.component.html',
  styleUrls: ['./edit-spot.component.css']
})
export class EditSpotComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: EditSpotDataSource;
  //p:number=1;
  //listeEditSpot:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  listeFloor:any[]=[]
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<EditSpotComponent>,
    private httpSv:HttpService,
  @Inject(MAT_DIALOG_DATA) public dataToUpdate) { 
    this.data=Object.assign({},dataToUpdate)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  editSpot(){
    if(this.data.number && this.data.floor_id){
        this.httpSv.update('spot',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Spot has been successfully edited')
            this.dialogRef.close({edited:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.number) this.snackBarService.openSnackBar('Enter spot number please');
      if(!this.data.floor_id) this.snackBarService.openSnackBar('Enter spot floor_id please');
    }
  }
  getFloor(){
    this.httpSv.geAll('floor').subscribe(res=>{
      this.listeFloor=res.items ? res.items : []
    },
  error=>{
    this.listeFloor=[]
  })
  }
  ngOnInit() {
    //this.dataSource = new EditSpotDataSource(this.paginator, this.sort);
    //this.getEditSpot()
    this.getFloor()
  }
}
