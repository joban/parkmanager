import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { HttpService } from '../api/http.service';
import { CreateSpotComponent } from './create-spot/create-spot.component';
import { EditSpotComponent } from './edit-spot/edit-spot.component';
import { ConfirmationDialogComponent } from '../confimation-dialog/confirmation-dialog.component';
import { SnackbarService } from '../service/snackbar/snackbar.service';

@Component({
  selector: 'app-spot',
  templateUrl: './spot.component.html',
  styleUrls: ['./spot.component.css']
})
export class SpotComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: SpotDataSource;
    listeSpot:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['number', 'floor','occupancy','occupied','action'];
  constructor(private httpSv:HttpService,public dialog: MatDialog,private snackBarService:SnackbarService){
  }
  getSpot(){
    this.httpSv.geAll('spot').subscribe(res=>{
      this.listeSpot=res.items ? res.items : []
    },
  error=>{
    this.listeSpot=[]
  })
  }
  deleteSpot(data){
    this.httpSv.delete('spot',data).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("Spot has been successfully deleted")
        this.getSpot()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  openCreateSpotModal(): void {
    const dialogRef = this.dialog.open(CreateSpotComponent, {
      width: '500px'//,
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      //this.animal = result;
      if(result.created) this.getSpot()
    });
  }
  openEditSpotModal(data): void {
    const dialogRef = this.dialog.open(EditSpotComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {

      if(result.edited) this.getSpot()
    });
  }
  confirmDeleteSpot(row): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: 'Are you sure to delete this spot ?'
    });

    dialogRef.beforeClosed().subscribe(result => {

      if(result) this.deleteSpot(row)
    });
  }
  ngOnInit() {
    //this.dataSource = new SpotDataSource(this.paginator, this.sort);
    this.getSpot()
  }
}
