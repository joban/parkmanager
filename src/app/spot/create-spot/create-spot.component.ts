import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-create-spot',
  templateUrl: './create-spot.component.html',
  styleUrls: ['./create-spot.component.css']
})
export class CreateSpotComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: CreateSpotDataSource;
  //p:number=1;
  //listeCreateSpot:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  listeFloor:any[]=[]
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<CreateSpotComponent>,
    private httpSv:HttpService,
  /*@Inject(MAT_DIALOG_DATA) public data*/) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createSpot(){
    if(this.data.number && this.data.floor_id){
        this.httpSv.create('spot',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Spot has been successfully created')
            this.dialogRef.close({created:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.number) this.snackBarService.openSnackBar('Enter spot number please');
      if(!this.data.floor_id) this.snackBarService.openSnackBar('Enter spot floor_id please');
    }
  }
  getFloor(){
    this.httpSv.geAll('floor').subscribe(res=>{
      this.listeFloor=res.items ? res.items : []
    },
  error=>{
    this.listeFloor=[]
  })
  }
  ngOnInit() {
    //this.dataSource = new CreateSpotDataSource(this.paginator, this.sort);
    //this.getCreateSpot()
    this.getFloor()
  }
}
