import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { HttpService } from '../api/http.service';
import { ConfirmationDialogComponent } from '../confimation-dialog/confirmation-dialog.component';
import { SnackbarService } from '../service/snackbar/snackbar.service';
import { dashCaseToCamelCase } from '@angular/animations/browser/src/util';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: ClientDataSource;
    listeClient:any[]=[];
    loggedUser:any={}
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['number', 'floor','occupied','action'];
  constructor(private httpSv:HttpService,public dialog: MatDialog,private snackBarService:SnackbarService){
    this.loggedUser=JSON.parse(sessionStorage.getItem('loggedUser'))
  }
  getClientSpot(){
    let data={
      user_id:this.loggedUser.id
    }
    this.httpSv.spotByUser('spot',data).subscribe(res=>{
      this.listeClient=res.items ? res.items : []
    },
  error=>{
    this.listeClient=[]
  })
  }
  manageClientAssignation(data){
    //data.occupied ? this.unassign(data) :this.assign(data)
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '500px',
        data: data.occupied ?'Are you sure to unassign this spot ?':'Are you sure to assign this spot ?'
      });
  
      dialogRef.beforeClosed().subscribe(result => {
  
        if(result) data.occupied ? this.unassign(data) :this.assign(data)
      });
  }
  assign(data){
    this.httpSv.assign('spot',Object.assign(data,{user_id:this.loggedUser.id})).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("Spot has been successfully assigned")
        this.getClientSpot()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  unassign(data){
    this.httpSv.unassign('spot',Object.assign(data,{user_id:this.loggedUser.id})).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("Spot has been successfully assigned")
        this.getClientSpot()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  ngOnInit() {
    //this.dataSource = new ClientDataSource(this.paginator, this.sort);
    this.getClientSpot()
  }
}
