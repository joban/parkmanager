import { Injectable,ViewChild,ViewContainerRef } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { catchError, tap } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';
const BASE_URL='http://127.0.0.1:3333/'
@Injectable()
export class HttpService {

  constructor(private http: HttpClient){}

  geAll(source: string) {
    return this.http.get(BASE_URL+source+'/getAll').pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  create(source: string,data) {
    return this.http.post(BASE_URL+source+'/create',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  getByCriteria(source: string,data) {
    return this.http.post(BASE_URL+source+'/getByCriteria',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  assign(source: string,data) {
    return this.http.post(BASE_URL+source+'/assign',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  unassign(source: string,data) {
    return this.http.post(BASE_URL+source+'/unassign',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  spotByUser(source: string,data) {
    return this.http.post(BASE_URL+source+'/spotByUser',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  update(source: string,data) {
    return this.http.post(BASE_URL+source+'/update',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  login(data) {
    return this.http.post(BASE_URL+'auth'+'/login',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  register(data) {
    return this.http.post(BASE_URL+'auth'+'/register',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  delete(source: string,data) {
    return this.http.post(BASE_URL+source+'/delete',data).pipe(
      tap((res: any) => res),
      catchError(this.handleError)
    );
  }
  private handleError(error: any) {
    return observableThrowError(error.error || 'Server error');
  }

}
