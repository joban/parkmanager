import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: CreateUserDataSource;
  //p:number=1;
  //listeCreateUser:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  listeRole:any[]=[]
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<CreateUserComponent>,
    private httpSv:HttpService,
  /*@Inject(MAT_DIALOG_DATA) public data*/) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createUser(){
    if(this.data.email && this.data.username && this.data.role_id && this.data.password){
        this.httpSv.register(this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('User has been successfully created')
            this.dialogRef.close({created:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.email) this.snackBarService.openSnackBar('Enter user email please');
      if(!this.data.password) this.snackBarService.openSnackBar('Enter user password please');
      if(!this.data.username) this.snackBarService.openSnackBar('Enter user username please');
      if(!this.data.role_id) this.snackBarService.openSnackBar('Enter user role_id please');
    }
  }
  getRole(){
    this.httpSv.geAll('role').subscribe(res=>{
      this.listeRole=res.items ? res.items : []
    },
  error=>{
    this.listeRole=[]
  })
  }
  ngOnInit() {
    //this.dataSource = new CreateUserDataSource(this.paginator, this.sort);
    //this.getCreateUser()
    this.getRole()
  }
}
