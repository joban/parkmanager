import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: EditUserDataSource;
  //p:number=1;
  //listeEditUser:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  listeRole:any[]=[]
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<EditUserComponent>,
    private httpSv:HttpService,
  @Inject(MAT_DIALOG_DATA) public dataToUpdate) { 
    this.data=Object.assign({},dataToUpdate)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  editUser(){
    if(this.data.email && this.data.username && this.data.role_id){
        this.httpSv.update('user',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('User has been successfully edited')
            this.dialogRef.close({edited:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.email) this.snackBarService.openSnackBar('Enter email email please');
      if(!this.data.username) this.snackBarService.openSnackBar('Enter username please');
      if(!this.data.role_id) this.snackBarService.openSnackBar('Enter user role_id please');
    }
  }
  getRole(){
    this.httpSv.geAll('role').subscribe(res=>{
      this.listeRole=res.items ? res.items : []
    },
  error=>{
    this.listeRole=[]
  })
  }
  ngOnInit() {
    //this.dataSource = new EditUserDataSource(this.paginator, this.sort);
    //this.getEditUser()
    this.getRole()
  }
}
