import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { HttpService } from '../api/http.service';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ConfirmationDialogComponent } from '../confimation-dialog/confirmation-dialog.component';
import { SnackbarService } from '../service/snackbar/snackbar.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: UserDataSource;
    listeUser:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['email', 'username','role','action'];
  constructor(private httpSv:HttpService,public dialog: MatDialog,private snackBarService:SnackbarService){
  }
  getUser(){
    this.httpSv.getByCriteria('user',{}).subscribe(res=>{
      this.listeUser=res.items ? res.items : []
    },
  error=>{
    this.listeUser=[]
  })
  }
  deleteUser(data){
    this.httpSv.delete('user',data).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("User has been successfully deleted")
        this.getUser()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  openCreateUserModal(): void {
    const dialogRef = this.dialog.open(CreateUserComponent, {
      width: '500px'//,
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      //this.animal = result;
      if(result.created) this.getUser()
    });
  }
  openEditUserModal(data): void {
    const dialogRef = this.dialog.open(EditUserComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {

      if(result.edited) this.getUser()
    });
  }
  confirmDeleteUser(row): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: 'Are you sure to delete this user ?'
    });

    dialogRef.beforeClosed().subscribe(result => {

      if(result) this.deleteUser(row)
    });
  }
  ngOnInit() {
    //this.dataSource = new UserDataSource(this.paginator, this.sort);
    this.getUser()
  }
}
