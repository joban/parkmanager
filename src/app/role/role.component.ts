import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { RoleDataSource } from './role-datasource';
import { HttpService } from '../api/http.service';
import { CreateRoleComponent } from './create-role/create-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { ConfirmationDialogComponent } from '../confimation-dialog/confirmation-dialog.component';
import { SnackbarService } from '../service/snackbar/snackbar.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: RoleDataSource;
    listeRole:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['code', 'value','action'];
  constructor(private httpSv:HttpService,public dialog: MatDialog,private snackBarService:SnackbarService){
  }
  getRole(){
    this.httpSv.geAll('role').subscribe(res=>{
      this.listeRole=res.items ? res.items : []
    },
  error=>{
    this.listeRole=[]
  })
  }
  deleteRole(data){
    this.httpSv.delete('role',data).subscribe(res=>{
      if(!res.error) {
        this.snackBarService.openSnackBar("Role has been successfully deleted")
        this.getRole()
      }
    },
  error=>{
    this.snackBarService.openSnackBar(error)
  })
  }
  openCreateRoleModal(): void {
    const dialogRef = this.dialog.open(CreateRoleComponent, {
      width: '500px'//,
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      //this.animal = result;
      if(result.created) this.getRole()
    });
  }
  openEditRoleModal(data): void {
    const dialogRef = this.dialog.open(EditRoleComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {

      if(result.edited) this.getRole()
    });
  }
  confirmDeleteRole(row): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '500px',
      data: 'Are you sure to delete this role ?'
    });

    dialogRef.beforeClosed().subscribe(result => {

      if(result) this.deleteRole(row)
    });
  }
  ngOnInit() {
    //this.dataSource = new RoleDataSource(this.paginator, this.sort);
    this.getRole()
  }
}
