import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.css']
})
export class EditRoleComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: EditRoleDataSource;
  //p:number=1;
  //listeEditRole:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<EditRoleComponent>,
    private httpSv:HttpService,
  @Inject(MAT_DIALOG_DATA) public dataToUpdate) { 
    this.data=Object.assign({},dataToUpdate)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  editRole(){
    if(this.data.code && this.data.value){
        this.httpSv.update('role',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Role has been successfully edited')
            this.dialogRef.close({edited:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.code) this.snackBarService.openSnackBar('Enter role code please');
      if(!this.data.value) this.snackBarService.openSnackBar('Enter role title please');
    }
  }
  ngOnInit() {
    //this.dataSource = new EditRoleDataSource(this.paginator, this.sort);
    //this.getEditRole()
  }
}
