import { AfterViewInit, Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../../api/http.service';
import { SnackbarService } from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css']
})
export class CreateRoleComponent implements OnInit {
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  // dataSource: CreateRoleDataSource;
  //p:number=1;
  //listeCreateRole:any[]=[]
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  data:any={}
  constructor(
    private snackBarService: SnackbarService,
    private dialogRef: MatDialogRef<CreateRoleComponent>,
    private httpSv:HttpService,
  /*@Inject(MAT_DIALOG_DATA) public data*/) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  createRole(){
    if(this.data.code && this.data.value){
        this.httpSv.create('role',this.data).subscribe(res=>{
          if(!res.error) {
            this.snackBarService.openSnackBar('Role has been successfully created')
            this.dialogRef.close({created:true})
          }
          else this.snackBarService.openSnackBar(res.message)
        },error=>{
          this.snackBarService.openSnackBar(error)
        })
    }
    else{
      if(!this.data.code) this.snackBarService.openSnackBar('Enter role code please');
      if(!this.data.value) this.snackBarService.openSnackBar('Enter role title please');
    }
  }
  ngOnInit() {
    //this.dataSource = new CreateRoleDataSource(this.paginator, this.sort);
    //this.getCreateRole()
  }
}
